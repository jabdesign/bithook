var $ = require('jquery');
var io = require('socket.io-client');
var socket = io();

socket.on('checkmate',function(data){
  if(typeof(data)!='object')
    return;
  console.log(data);
  data = data.response;
  $('.wrap').empty();
  data.forEach(function(job){
    $('.wrap').append('<div class="info">- push');
      job = job.push.changes;
      job.forEach(function(p){
        p = p.commits;
        p.forEach(function(c){
          $('.wrap').append('<div class="info2">- - - - commit <div class="button">DEPLOY</div> : '+c.message+'<textarea disabled>'+c.author.raw+'</textarea>');
        });
      });
  });
});

$(document).on('click','.button',function(){
  $.get(window.location.protocol+'//'+window.location.host+'/deploy');
});
