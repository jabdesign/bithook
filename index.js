var path = require('path');
var util = require('util');
var express = require('express');
var http = require('http').Server(app);
var io = require('socket.io')(http);
var bodyParser = require('body-parser')
var fs = require('fs');

var app = express();
console.log(path.join(__dirname, '/public'));
app.use(express.static(path.join(__dirname, '/public/')));
app.use( bodyParser.json() );       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
      extended: true
})); 
var app_paths = {
  client:__dirname,
  server:'./'
};

var queue = [];
var users = [];

app.get('/', function(req, res){
  res.sendFile(__dirname+'/index.html');
});
app.get('/node',function(req, res){
  res.json({message:'no commits yet..'});
});
app.get('/log',function(req,res){
  res.json(JSON.stringify(queue));
});
app.all('/trigger', function(req,res){
  console.log('got triggered');
  console.log(req.body);
  var data = req.body;
  res.end();
  queue.push(data);
  io.emit('checkmate',{response:queue});
});
app.get('/test',function(req,res){
  queue.push({"push":{"changes":[{"links":{"commits":{"href":"https://api.bitbucket.org/2.0/repositories/jabdesign/bithook/commits?include=df392f82419ba9742848169d3b1b2f131d151d54&exclude=0dd7d28453e73627cc068e9c91061a39a9b3e507"},"diff":{"href":"https://api.bitbucket.org/2.0/repositories/jabdesign/bithook/diff/df392f82419ba9742848169d3b1b2f131d151d54..0dd7d28453e73627cc068e9c91061a39a9b3e507"},"html":{"href":"https://bitbucket.org/jabdesign/bithook/branches/compare/df392f82419ba9742848169d3b1b2f131d151d54..0dd7d28453e73627cc068e9c91061a39a9b3e507"}},"forced":false,"new":{"links":{"self":{"href":"https://api.bitbucket.org/2.0/repositories/jabdesign/bithook/refs/branches/master"},"commits":{"href":"https://api.bitbucket.org/2.0/repositories/jabdesign/bithook/commits/master"},"html":{"href":"https://bitbucket.org/jabdesign/bithook/branch/master"}},"target":{"type":"commit","hash":"df392f82419ba9742848169d3b1b2f131d151d54","message":"write to file\n","links":{"self":{"href":"https://api.bitbucket.org/2.0/repositories/jabdesign/bithook/commit/df392f82419ba9742848169d3b1b2f131d151d54"},"html":{"href":"https://bitbucket.org/jabdesign/bithook/commits/df392f82419ba9742848169d3b1b2f131d151d54"}},"parents":[{"links":{"self":{"href":"https://api.bitbucket.org/2.0/repositories/jabdesign/bithook/commit/0dd7d28453e73627cc068e9c91061a39a9b3e507"},"html":{"href":"https://bitbucket.org/jabdesign/bithook/commits/0dd7d28453e73627cc068e9c91061a39a9b3e507"}},"type":"commit","hash":"0dd7d28453e73627cc068e9c91061a39a9b3e507"}],"author":{"raw":"Ubuntu <ubuntu@ip-172-31-32-76.us-west-2.compute.internal>"},"date":"2015-10-19T16:48:05+00:00"},"name":"master","type":"branch"},"created":false,"old":{"links":{"self":{"href":"https://api.bitbucket.org/2.0/repositories/jabdesign/bithook/refs/branches/master"},"commits":{"href":"https://api.bitbucket.org/2.0/repositories/jabdesign/bithook/commits/master"},"html":{"href":"https://bitbucket.org/jabdesign/bithook/branch/master"}},"target":{"type":"commit","hash":"0dd7d28453e73627cc068e9c91061a39a9b3e507","message":"init\n","links":{"self":{"href":"https://api.bitbucket.org/2.0/repositories/jabdesign/bithook/commit/0dd7d28453e73627cc068e9c91061a39a9b3e507"},"html":{"href":"https://bitbucket.org/jabdesign/bithook/commits/0dd7d28453e73627cc068e9c91061a39a9b3e507"}},"parents":[],"author":{"raw":"Jabran Saeed <jabransaeed.m@gmail.com>","user":{"username":"jabdesign","display_name":"Jabran Saeed","uuid":"{4b436a64-f562-4edd-a724-97c77f8c0218}","type":"user","links":{"self":{"href":"https://api.bitbucket.org/2.0/users/jabdesign"},"avatar":{"href":"https://bitbucket.org/account/jabdesign/avatar/32/"},"html":{"href":"https://bitbucket.org/jabdesign/"}}}},"date":"2015-10-19T16:05:55+00:00"},"name":"master","type":"branch"},"closed":false,"commits":[{"links":{"self":{"href":"https://api.bitbucket.org/2.0/repositories/jabdesign/bithook/commit/df392f82419ba9742848169d3b1b2f131d151d54"},"html":{"href":"https://bitbucket.org/jabdesign/bithook/commits/df392f82419ba9742848169d3b1b2f131d151d54"}},"type":"commit","author":{"raw":"Ubuntu <ubuntu@ip-172-31-32-76.us-west-2.compute.internal>"},"message":"write to file\n","hash":"df392f82419ba9742848169d3b1b2f131d151d54"}],"truncated":false}]},"actor":{"username":"jabdesign","display_name":"Jabran Saeed","uuid":"{4b436a64-f562-4edd-a724-97c77f8c0218}","type":"user","links":{"self":{"href":"https://api.bitbucket.org/2.0/users/jabdesign"},"avatar":{"href":"https://bitbucket.org/account/jabdesign/avatar/32/"},"html":{"href":"https://bitbucket.org/jabdesign/"}}},"repository":{"owner":{"username":"jabdesign","display_name":"Jabran Saeed","uuid":"{4b436a64-f562-4edd-a724-97c77f8c0218}","type":"user","links":{"self":{"href":"https://api.bitbucket.org/2.0/users/jabdesign"},"avatar":{"href":"https://bitbucket.org/account/jabdesign/avatar/32/"},"html":{"href":"https://bitbucket.org/jabdesign/"}}},"scm":"git","full_name":"jabdesign/bithook","name":"bithook","links":{"self":{"href":"https://api.bitbucket.org/2.0/repositories/jabdesign/bithook"},"avatar":{"href":"https://bitbucket.org/jabdesign/bithook/avatar/32/"},"html":{"href":"https://bitbucket.org/jabdesign/bithook"}},"uuid":"{782c339e-3e31-491a-a78e-0b15d90db214}","is_private":false,"type":"repository"}});
  io.emit('checkmate',{response:queue});
  res.end();
});
app.get('/deploy',function(){
  if(queue.length>0)
    queue.splice(0,1);
  io.emit('checkmate',{response:queue});
});
// REALTIME
io.on('connection', function(socket){
  console.log('a user connected');
  io.emit('checkmate',{response:queue});
});


var _port = 3003;
//process.env.OPENSHIFT_NODEJS_PORT || process.env.PORT || 3000;
var _host = process.env.OPENSHIFT_NODEJS_IP || '0.0.0.0';
var server = app.listen(_port,  _host, function () {
  var host = server.address().address;
  var port = server.address().port;
  console.log('\nbithook v0.1'.green);
  console.log('JabDesign/2015 '.grey)
  console.log('\nListining at '+port);
});
io.listen(server);